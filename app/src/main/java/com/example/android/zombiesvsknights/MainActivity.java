package com.example.android.zombiesvsknights;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // default for zombies
    private final int zombiesPopDefault = 50;
    private final double zombieAttackLight = 0.1;
    private final double zombieAttackMedium = 0.3;
    private final double zombieAttackStrong = 0.5;
    private final double zombieLeechFactor = 1;

    // defaults for knights
    private final int knightsPopDefault = 1000;
    private final double knightAttackLight = 0.01;
    private final double knightAttackMedium = 0.03;
    private final double knightAttackStrong = 0.05;

    // further balance
    private final double defenderFactor = 0.7;
    private final double attackerFactor = 1.2;

    // variables to be used for the game state
    private int zombiePopValue = zombiesPopDefault;
    private int knightPopValue = knightsPopDefault;
    private boolean isGameOver = false;
    private boolean isZombieWin = false;
    private int gameOverSince = 0;
    private int panicLevel = 0;
    private List<String> zombieToasts = new ArrayList<>();
    private List<String> knightToasts = new ArrayList<>();
    private boolean isPort = false;
    private String attackText = "";

    // References to be saved for later use
    TextView zombiePopView;
    ImageView zombieBonus;
    Button zombieBtnStrong;
    Button zombieBtnMedium;
    Button zombieBtnLight;
    TextView knightPopView;
    ImageView knightBonus;
    Button knightBtnStrong;
    Button knightBtnMedium;
    Button knightBtnLight;
    TextView attackMessageView;
    Toast gameOverToast;

    LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        setReferences();
        isPort = this.getResources().getBoolean(R.bool.is_portrait);
        setupGame();
        checkBuffState();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("zombiesPopValue", zombiePopValue);
        outState.putInt("knightPopValue", knightPopValue);
        outState.putBoolean("isGameOver", isGameOver);
        outState.putBoolean("isZombieWin", isZombieWin);
        outState.putInt("gameOverSince", gameOverSince);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        zombiePopValue = savedInstanceState.getInt("zombiesPopValue", 0);
        knightPopValue = savedInstanceState.getInt("knightPopValue", 0);
        isGameOver = savedInstanceState.getBoolean("isGameOver");
        isZombieWin = savedInstanceState.getBoolean("isZombieWin");
        gameOverSince = savedInstanceState.getInt("gameOverSince", 0);
        checkGameState();
        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Called once at startup. Sets all the references so the lookups don't need to be done twice.
     */
    private void setReferences() {
        layout = findViewById(R.id.root_layout);
        zombiePopView = findViewById(R.id.zombie_pop);
        knightPopView = findViewById(R.id.knight_pop);
        zombieBonus = findViewById(R.id.zombies_bonus);
        knightBonus = findViewById(R.id.knights_bonus);
        zombieBtnStrong = findViewById(R.id.zombie_strong);
        zombieBtnMedium = findViewById(R.id.zombie_medium);
        zombieBtnLight = findViewById(R.id.zombie_light);
        knightBtnStrong = findViewById(R.id.knight_strong);
        knightBtnMedium = findViewById(R.id.knight_medium);
        knightBtnLight = findViewById(R.id.knight_light);
        attackMessageView = findViewById(R.id.attack_message);
    }

    /**
     * Called once at startup. Sets UI values that are more complicated then a simple strings.xml
     * can handle.
     */
    private void setupGame() {
        zombieBtnStrong.setText((int) (zombieAttackStrong * 100) + "%");
        zombieBtnMedium.setText((int) (zombieAttackMedium * 100) + "%");
        zombieBtnLight.setText((int) (zombieAttackLight * 100) + "%");

        zombieToasts.add(getResources().getString(R.string.zombieToast1));
        zombieToasts.add(getResources().getString(R.string.zombieToast2));
        zombieToasts.add(getResources().getString(R.string.zombieToast3));
        zombieToasts.add(getResources().getString(R.string.zombieToast4));
        zombieToasts.add(getResources().getString(R.string.zombieToast5));

        knightBtnStrong.setText((int) (knightAttackStrong * 100) + "%");
        knightBtnMedium.setText((int) (knightAttackMedium * 100) + "%");
        knightBtnLight.setText((int) (knightAttackLight * 100) + "%");

        knightToasts.add(getResources().getString(R.string.knightToast1));
        knightToasts.add(getResources().getString(R.string.knightToast2));
        knightToasts.add(getResources().getString(R.string.knightToast3));
        knightToasts.add(getResources().getString(R.string.knightToast4));
        knightToasts.add(getResources().getString(R.string.knightToast5));

        drawValues();
    }

    /**
     * Animates a background change in color / gradient
     *
     * @param newDraw the new gradient to animate to
     */
    private void setBackground(Drawable newDraw) {
        Drawable currentDraw = layout.getBackground();
        TransitionDrawable transitionDrawable = new TransitionDrawable(new Drawable[]{currentDraw, newDraw});
        transitionDrawable.setCrossFadeEnabled(true);
        layout.setBackground(transitionDrawable);
        transitionDrawable.startTransition(1500);
    }

    /**
     * issues a strong zombie attack
     *
     * @param v
     */
    protected void hitStrongZombie(View v) {
        zombieAttack(zombieAttackStrong, R.string.zombieAttackStrong);
    }

    /**
     * issues a medium zombie attack
     *
     * @param v
     */
    protected void hitMediumZombie(View v) {
        zombieAttack(zombieAttackMedium, R.string.zombieAttackMedium);
    }

    /**
     * issues a light zombie attack
     *
     * @param v
     */
    protected void hitLightZombie(View v) {
        zombieAttack(zombieAttackLight, R.string.zombieAttackLight);
    }

    /**
     * executes a zombie attack if game is not over yet. Will show a Toast otherwise
     *
     * @param attackStr strong, medium, light?
     */
    private void zombieAttack(double attackStr, int attackID) {
        if (isGameOver) {
            showToast();
        } else {
            if (zombiePopValue == 1) {
                attackText = "This last one is a funny one.\nLook how he tries to fight! Ha!\nThey let him stay, for now.";
            } else {
                double bonus = 1;
                if (isPort && zombiePopValue > knightPopValue) {
                    bonus = attackerFactor;
                } else if (!isPort && zombiePopValue > knightPopValue) {
                    bonus = defenderFactor;
                }
                int attackValue = (int) Math.floor(zombiePopValue * attackStr * bonus);
                knightPopValue -= attackValue;
                zombiePopValue += Math.floor(attackValue * zombieLeechFactor);

                attackText = String.format(getResources().getString(attackID), attackValue);
            }
            checkGameState();
        }
    }

    /**
     * issues a strong knight attack
     *
     * @param v
     */
    protected void hitStrongKnight(View v) {
        knightAttack(knightAttackStrong, R.string.knightAttackStrong);
    }

    /**
     * issues a medium knight attack
     *
     * @param v
     */
    protected void hitMediumKnight(View v) {
        knightAttack(knightAttackMedium, R.string.knightAttackMedium);
    }

    /**
     * issues a light knight attack
     *
     * @param v
     */
    protected void hitLightKnight(View v) {
        knightAttack(knightAttackLight, R.string.knightAttackLight);
    }

    /**
     * executes a knight attack if game is not over yet. Will show a Toast otherwise
     *
     * @param attackStr strong, medium, light?
     */
    private void knightAttack(double attackStr, int attackID) {
        if (isGameOver) {
            showToast();
        } else {
            double bonus = 1;
            if (isPort && zombiePopValue < knightPopValue) {
                bonus = attackerFactor;
            } else if (!isPort && zombiePopValue < knightPopValue) {
                bonus = defenderFactor;
            }
            int attackValue = (int) Math.floor(knightPopValue * attackStr * bonus);
            zombiePopValue -= attackValue;

            attackText = String.format(getResources().getString(attackID), attackValue);
            checkGameState();
        }
    }

    /**
     * checks for the game state and calls proper methods depending on that
     */
    private void checkGameState() {
        // check if any team won
        if (knightPopValue <= 0) {
            isGameOver = true;
            isZombieWin = true;
            toggleButtons(false);
            showToast();
        } else if (zombiePopValue <= 0) {
            isGameOver = true;
            toggleButtons(false);
            showToast();
        }
        // check how severe the knights are in danger
        if (knightPopValue < zombiePopValue * 2) {
            // if knights have less then two times the numbers it is really severe
            if (panicLevel != 3) {
                setBackground(getResources().getDrawable(R.drawable.gradient_3, null));
                panicLevel = 3;
            }
        } else if (knightPopValue < zombiePopValue * 10) {
            // less then 10 times the numbers, things are getting real.
            if (panicLevel != 2) {
                setBackground(getResources().getDrawable(R.drawable.gradient_2, null));
                panicLevel = 2;
            }
        } else if (knightPopValue < knightsPopDefault) {
            // if pop is lost, the zombies have been noticed
            if (panicLevel != 1) {
                setBackground(getResources().getDrawable(R.drawable.gradient_1, null));
                panicLevel = 1;
            }
        }
        checkBuffState();
        drawValues();
    }

    /**
     * checks for any bonuses that should be active
     */
    private void checkBuffState() {
        if (isPort) {
            if (zombiePopValue > knightPopValue) {
                zombieBonus.setImageResource(R.drawable.ic_flash_on_black_24dp);
                zombieBonus.setVisibility(View.VISIBLE);
                knightBonus.setVisibility(View.INVISIBLE);
            } else {
                knightBonus.setImageResource(R.drawable.ic_flash_on_black_24dp);
                knightBonus.setVisibility(View.VISIBLE);
                zombieBonus.setVisibility(View.INVISIBLE);
            }
        } else if (!isPort) {
            if (zombiePopValue < knightPopValue) {
                zombieBonus.setImageResource(R.drawable.ic_security_black_24dp);
                zombieBonus.setVisibility(View.VISIBLE);
                knightBonus.setVisibility(View.INVISIBLE);
            } else {
                knightBonus.setImageResource(R.drawable.ic_security_black_24dp);
                knightBonus.setVisibility(View.VISIBLE);
                zombieBonus.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * redraws the visible values that can change
     */
    private void drawValues() {
        zombiePopView.setText(String.valueOf(zombiePopValue));
        knightPopView.setText(String.valueOf(knightPopValue));
        attackMessageView.setText(attackText);
    }

    /**
     * toggles buttons to off for the loser, or on for everyone.
     *
     * @param toggle
     */
    private void toggleButtons(boolean toggle) {
        if (toggle) {
            zombieBtnStrong.setEnabled(toggle);
            zombieBtnMedium.setEnabled(toggle);
            zombieBtnLight.setEnabled(toggle);
            knightBtnStrong.setEnabled(toggle);
            knightBtnMedium.setEnabled(toggle);
            knightBtnLight.setEnabled(toggle);
        } else {
            if (isZombieWin) {
                knightBtnStrong.setEnabled(toggle);
                knightBtnMedium.setEnabled(toggle);
                knightBtnLight.setEnabled(toggle);
            } else {
                zombieBtnStrong.setEnabled(toggle);
                zombieBtnMedium.setEnabled(toggle);
                zombieBtnLight.setEnabled(toggle);
            }
        }
    }

    /**
     * shows a Toast depending on who the winner is. Gets the messages from a list of messages.
     */
    private void showToast() {
        if (isZombieWin) {
            gameOverToast = Toast.makeText(this, zombieToasts.get(gameOverSince), Toast.LENGTH_LONG);
            if (gameOverSince + 1 < zombieToasts.size()) {
                gameOverSince++;
            }
        } else {
            gameOverToast = Toast.makeText(this, knightToasts.get(gameOverSince), Toast.LENGTH_LONG);
            if (gameOverSince + 1 < knightToasts.size()) {
                gameOverSince++;
            }
        }
        gameOverToast.show();
    }

    /**
     * Resets the game.
     *
     * @param v
     */
    protected void resetTime(View v) {
        zombiePopValue = zombiesPopDefault;
        knightPopValue = knightsPopDefault;
        attackText = "";
        isGameOver = false;
        gameOverSince = 0;
        isZombieWin = false;
        panicLevel = 0;
        drawValues();
        setBackground(getResources().getDrawable(R.drawable.gradient_0, null));
        toggleButtons(true);
    }
}
